
#include <iostream>
#include <cmath>

using namespace std;

class Vector 
{
    float x, y, z;
public:
    Vector() : x(0), y(0), z(0)
    { 
     
    }
    Vector(float _x, float _y, float _z) : x(_x), y(_y), z(_z)
    {

    }
    void show_vector() 
    {
        cout << "v : (" << x << ", " << y << ", " << z << ")" << endl;
    }

    double get_module() 
    {
        return sqrt(x * x + y * y + z * z);
    }
};

int main()
{
    Vector vec(0.7, 2.1, 5.3);

    vec.show_vector();

    cout << "|v| : " << vec.get_module() << endl;
}
